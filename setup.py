from setuptools import find_packages, setup

import os
from glob import glob

package_name = 'avp_ros2_bringup_demo'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='kaijunge',
    maintainer_email='kai.junge@epfl.ch',
    description='A simple package to display the transforms of your hands, wrists, and head from the AVP.',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'avp_demo_node = avp_ros2_bringup_demo.avp_demo_node:main'
        ],
    },
)
