import rclpy
from rclpy.node import Node
from scipy.spatial.transform import Rotation as R
from tf2_ros import TransformBroadcaster
from geometry_msgs.msg import TransformStamped
from avp_stream import VisionProStreamer

class AVPDemoNode(Node):

    def __init__(self):
        super().__init__('avp_demo_node')

        # tf related
        self.tf_broadcaster = TransformBroadcaster(self)

        # avp related
        self.avp = VisionProStreamer(ip = "YOUR IP ADDRESS", record = True)

        # Main loop
        self.mainloop = self.create_timer(0.05, self.mainloop_callback) 
    
    def broadcast_avp_frame(self, base_name, frame_name, data):
        rot_matrix = data[:3,:3]
        trans = data[:3, 3]

        quat = R.as_quat(R.from_matrix(rot_matrix))

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = base_name
        t.child_frame_id = frame_name

        t.transform.translation.x = trans[0]
        t.transform.translation.y = trans[1]
        t.transform.translation.z = trans[2]

        t.transform.rotation.x = quat[0]
        t.transform.rotation.y = quat[1]
        t.transform.rotation.z = quat[2]
        t.transform.rotation.w = quat[3]

        self.tf_broadcaster.sendTransform(t)
    
    def mainloop_callback(self):
        r = self.avp.latest

        self.broadcast_avp_frame("world", "head", r["head"][0])

        self.broadcast_avp_frame("world", "wrist_l", r["left_wrist"][0])

        self.broadcast_avp_frame("world", "wrist_r", r["right_wrist"][0])

        frame_names = [ "base", "thumb_cmc", "thumb_mcp", "thumb_ip", "thumb_tip",
                        "index_base", "index_mcp", "index_pip", "index_dip", "index_tip",
                        "middle_base", "middle_mcp", "middle_pip", "middle_dip", "middle_tip",
                        "ring_base", "ring_mcp", "ring_pip", "ring_dip", "ring_tip",
                        "pinky_base", "pinky_mcp", "pinky_pip", "pinky_dip", "pinky_tip"]
                
        # Right hand
        for i, name in enumerate(frame_names):
            self.broadcast_avp_frame("wrist_r", name + "_r", r["right_fingers"][i])

        # Left hand
        for i, name in enumerate(frame_names):
            self.broadcast_avp_frame("wrist_l", name + "_l", r["left_fingers"][i])
        
        
def main():
    rclpy.init()
    node = AVPDemoNode()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()