# AVP ROS2 Bringup Demo

A minimal ROS2 wrapper package on the [VisionProTeleop](https://github.com/Improbable-AI/VisionProTeleop), to display all the coordinate frames through tf2. 

## How to use it

1. Clone/download this repo into your ros2 workspace
2. Follow the steps for installation [here](https://github.com/Improbable-AI/VisionProTeleop) to get the app on your vision pro device alongside installing the `avp_stream` python package. 
3. Change `YOUR IP ADDRESS` (in `avp_ros2_bringup_demo/avp_demo_node.py`) to the ip address of the vision pro. 
4. After building/sourcing, `ros2 launch avp_ros2_bringup_demo bringup.launch.py`

Have fun!